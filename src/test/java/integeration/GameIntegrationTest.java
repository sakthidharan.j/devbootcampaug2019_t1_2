package integeration;

import Player.Player;
import game.Game;
import machine.Machine;
import move.CheatMove;
import move.CooperativeMove;
import move.CopyCatMove;
import move.GrudgerMove;
import org.junit.jupiter.api.Test;

public class GameIntegrationTest {


    @Test
    public void integrationGrugder(){
        GrudgerMove grudgerMoveStrategy = new GrudgerMove();
        CooperativeMove copycatMoveStrategy = new CooperativeMove();

        Player grudger = new Player(0, grudgerMoveStrategy);
        Player copycat = new Player(0, copycatMoveStrategy);
        Game game = new Game(grudger, copycat, new Machine(), 10, System.out);

        game.addObserver(grudgerMoveStrategy);
        //game.addObserver(copycatMoveStrategy);

        game.play();
    }
}
