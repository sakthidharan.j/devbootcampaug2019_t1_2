package unittest;

import Player.Player;
import Player.PlayerType;
import move.CheatMove;
import move.CooperativeMove;
import move.ManualMove;
import move.Move;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerTest {

    Player player;

    @Test
    public void shouldMoveManually() throws IOException {
        player = Mockito.mock(Player.class);
        Mockito.when(player.makeAMove()).thenReturn(Move.CHEAT);
        assertEquals(Move.CHEAT, player.makeAMove());
    }

    @Test
    public void shouldMoveWhenCheat() throws IOException {
        player = new Player(0, new CheatMove());
        assertEquals(Move.CHEAT, player.makeAMove());
    }

    @Test
    public void shouldMoveWhenCoop() throws IOException {
        player = new Player(0, new CooperativeMove());
        assertEquals(Move.COOPERATE, player.makeAMove());
    }

    @Test
    public void shouldInitializeScore(){
        player = new Player(0, new CheatMove());
        assertEquals(0, player.getScore());
    }

    @Test
    public void shouldAddScore(){
        player = new Player(0, new CheatMove());
        player.addScore(3);
        assertEquals(3, player.getScore());
    }

}
