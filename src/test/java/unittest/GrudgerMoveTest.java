package unittest;

import Player.Player;
import move.CopyCatMove;
import move.GrudgerMove;
import move.Move;
import org.javatuples.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Observable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GrudgerMoveTest {

    @Test
    public void shouldCreateInstance() {
        Observable observableGame = mock(Observable.class);

        GrudgerMove grudgerMove = new GrudgerMove();
        Assertions.assertNotNull(grudgerMove);
    }

    @Test
    public void shouldReturnCooperateUntilCooperateIsNotified() {

        // Arrange
        Observable observableGame = mock(Observable.class);
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        GrudgerMove grudgerMove = new GrudgerMove();

        when(player1.getMoveStrategy()).thenReturn(grudgerMove);

        Pair<Player, Move>[] message = new Pair[]{new Pair(player1, Move.COOPERATE), new Pair(player2, Move.CHEAT)};

        // Act
        grudgerMove.update(observableGame, message);

        // Assert
        assertEquals(grudgerMove.makeAMove(), Move.CHEAT);
    }

    @Test
    public void shouldReturnAlwaysCheatOnceCheatIsNotified() {

        // Arrange
        Observable observableGame = mock(Observable.class);
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        GrudgerMove grudgerMove = new GrudgerMove();

        when(player1.getMoveStrategy()).thenReturn(grudgerMove);

        Pair<Player, Move>[] message1 = new Pair[]{new Pair(player1, Move.COOPERATE), new Pair(player2, Move.COOPERATE)};
        Pair<Player, Move>[] message2 = new Pair[]{new Pair(player1, Move.COOPERATE), new Pair(player2, Move.CHEAT)};

        // Act
        grudgerMove.update(observableGame, message1);
        assertEquals(Move.COOPERATE, grudgerMove.makeAMove());

        grudgerMove.update(observableGame, message2);
        assertEquals(Move.CHEAT, grudgerMove.makeAMove());
    }
}
