package unittest;

import machine.Machine;
import machine.Score;
import move.Move;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MachineTest {
    Machine machine;

    @BeforeEach
    public void shouldMachineExists(){
        machine = new Machine();
    }

    @ParameterizedTest
    @MethodSource("movesInput")
    void testMachineCalucation(Input in) {
        Score score = machine.calculateScore(in.player1Move, in.player2Move);
        assertEquals(in.expectedScore.getPlayerOneScore(), score.getPlayerOneScore());
        assertEquals(in.expectedScore.getPlayerTwoScore(), score.getPlayerTwoScore());
    }

    static class Input{
        Move player1Move;
        Move player2Move;
        Score expectedScore;
        public Input(Move m1, Move m2, Score expectedScore){
            this.player1Move=m1;
            this.player2Move=m2;
            this.expectedScore=expectedScore;
        }
    }

    static Input[] movesInput(){
        return new Input[]{
                new Input(Move.COOPERATE, Move.COOPERATE, new Score(2,2 )),
                new Input(Move.COOPERATE, Move.CHEAT, new Score(-1,3)),
                new Input(Move.CHEAT, Move.COOPERATE, new Score(3, -1)),
                new Input(Move.CHEAT, Move.CHEAT, new Score(0,0))
        };
    }





}
