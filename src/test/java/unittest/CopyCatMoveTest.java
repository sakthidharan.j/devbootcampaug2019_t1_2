package unittest;

import Player.Player;
import move.CopyCatMove;
import move.Move;
import org.junit.jupiter.api.Test;
import org.javatuples.Pair;


import java.util.Observable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class CopyCatMoveTest {

    @Test
    public void shouldReturnCooperateOnFirstMove() {
        Observable observable = mock(Observable.class);
        CopyCatMove copyCatMove = new CopyCatMove();

        assertEquals(copyCatMove.makeAMove(), Move.COOPERATE);
    }


    @Test
    public void shouldReturnMoveObserved() {

        Observable observableGame = mock(Observable.class);

        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);

        CopyCatMove copyCatMove = new CopyCatMove();

        when(player1.getMoveStrategy()).thenReturn(copyCatMove);

        Pair<Player, Move>[] message = new Pair[]{new Pair<Player, Move>(player1, Move.COOPERATE),
                new Pair<Player, Move>(player2, Move.CHEAT)};

        copyCatMove.update(observableGame, message);

        assertEquals(copyCatMove.makeAMove(), Move.CHEAT);

    }


    @Test
    public void shouldReturnMoveObservedMulitpleRounds() {

        Observable observableGame = mock(Observable.class);

        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);

        CopyCatMove copyCatMove = new CopyCatMove();

        when(player1.getMoveStrategy()).thenReturn(copyCatMove);

        Pair<Player, Move>[] message = new Pair[]{new Pair<Player, Move>(player1, Move.COOPERATE),
                new Pair<Player, Move>(player2, Move.CHEAT)};

        copyCatMove.update(observableGame, message);

        assertEquals(copyCatMove.makeAMove(), Move.CHEAT);

    }


}
