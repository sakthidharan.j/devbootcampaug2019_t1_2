package unittest;

import Player.Player;
import Player.PlayerType;
import game.Game;
import machine.Machine;
import machine.Score;
import move.CheatMove;
import move.CooperativeMove;
import move.Move;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.Observer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;


public class GameTest {

    Player player1;
    Player player2;
    Machine machine;
    PrintStream console;
    BufferedReader br;

    @BeforeEach
    public void setUp() {
        player1 = mock(Player.class);
        player2 = mock(Player.class);
        machine = mock(Machine.class);
        console = mock(PrintStream.class);
        br = mock(BufferedReader.class);
    }

    @Test
    public void shouldPlayOneRound() {

        Game game = new Game(player1, player2, machine, 1, console);
        when(player1.makeAMove()).thenReturn(Move.COOPERATE);
        when(player2.makeAMove()).thenReturn(Move.COOPERATE);
        when(machine.calculateScore(Move.COOPERATE, Move.COOPERATE)).thenReturn(new Score(2, 2));
        when(player1.getScore()).thenReturn(2);
        when(player2.getScore()).thenReturn(2);
        game.play();

        Mockito.verify(player1).makeAMove();
        Mockito.verify(player2).makeAMove();
        Mockito.verify(machine).calculateScore(Move.COOPERATE, Move.COOPERATE);
        Mockito.verify(console).println("machine.Score 2 2");

    }

    @Test
    public void shouldPlayMultipleRound() {

        Game game = new Game(player1, player2, machine, 2, console);

        when(player1.makeAMove()).thenReturn(Move.COOPERATE);
        when(player2.makeAMove()).thenReturn(Move.COOPERATE);
        when(machine.calculateScore(Move.COOPERATE, Move.COOPERATE)).
                thenReturn(new Score(2, 2)).
                thenReturn(new Score(2, 2));
        when(player1.getScore()).thenReturn(2).thenReturn(4);
        when(player2.getScore()).thenReturn(2).thenReturn(4);
        game.play();

        Mockito.verify(player1, Mockito.times(2)).makeAMove();
        Mockito.verify(player2, Mockito.times(2)).makeAMove();
        Mockito.verify(machine, Mockito.times(2)).calculateScore(Move.COOPERATE, Move.COOPERATE);
        Mockito.verify(console).println("machine.Score 2 2");
        Mockito.verify(console).println("machine.Score 4 4");
    }

    @Test
    public void shouldPlayMultipleRoundsWithAllCheatAndAllCoop() {

        Player player1 = new Player(0, new CheatMove());
        Player player2 = new Player(0, new CooperativeMove());

        Game game = new Game(player1, player2, new Machine(), 5, console);
        game.play();
        Mockito.verify(console).println("machine.Score 3 -1");
        Mockito.verify(console).println("machine.Score 6 -2");
        Mockito.verify(console).println("machine.Score 9 -3");
        Mockito.verify(console).println("machine.Score 12 -4");
        Mockito.verify(console).println("machine.Score 15 -5");
    }


    @Test
    public void shouldInvokeNotifyAllObserversAfterOneRoundIsCompleted() {
        // Arrange
        Observer observer = mock(Observer.class);

        when(this.player1.makeAMove()).thenReturn(Move.COOPERATE);
        when(this.player2.makeAMove()).thenReturn(Move.CHEAT);

        when(this.machine.calculateScore(any(), any())).thenReturn(new Score(-1, 3));


        int noOfRounds = 1;

        Game game = new Game(this.player1, this.player2, this.machine, noOfRounds, this.console);
        game.addObserver(observer);

        // Act
        game.play();

        // Assert
        verify(observer, times(1))
                .update(game, new Pair[]{new Pair<Player, Move>(this.player1, Move.COOPERATE),
                        new Pair<Player, Move>(this.player2, Move.CHEAT)});
    }


    @Test
    public void shouldInvokeNotifyAllObserversAfterEveryRound() {
        // Arrange
        Observer observer = mock(Observer.class);

        when(this.player1.makeAMove()).thenReturn(Move.COOPERATE);
        when(this.player2.makeAMove()).thenReturn(Move.CHEAT);
        when(this.machine.calculateScore(any(), any())).thenReturn(new Score(-1, 3));

        int noOfRounds = 5;

        Game game = new Game(this.player1, this.player2, this.machine, noOfRounds, this.console);
        game.addObserver(observer);

        // Act
        game.play();

        // Assert
        verify(observer, times(5))
                .update(game, new Pair[]{new Pair<Player, Move>(this.player1, Move.COOPERATE),
                        new Pair<Player, Move>(this.player2, Move.CHEAT)});


    }




}
