package machine;

import move.Move;

public class Machine {

    public Score calculateScore(Move firstPlayerMove, Move secondPlayerMove) {

        if(firstPlayerMove.equals(Move.COOPERATE) && secondPlayerMove.equals(Move.COOPERATE)){
            return new Score(2,2);
        }
        else if(firstPlayerMove.equals(Move.COOPERATE) && secondPlayerMove.equals(Move.CHEAT)){
            return new Score(-1,3);
        }
        else if(firstPlayerMove.equals(Move.CHEAT) && secondPlayerMove.equals(Move.COOPERATE)){
            return new Score(3,-1);
        }
        else{
            return new Score(0,0);
        }
    }
}
