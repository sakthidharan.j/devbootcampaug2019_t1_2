package game;

import Player.Player;
import machine.Machine;
import machine.Score;
import move.Move;
import org.javatuples.Pair;

import java.io.PrintStream;
import java.util.*;

public class Game extends Observable {


    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfRounds;
    private final PrintStream console;
    private ArrayList<Observer> observers;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds, PrintStream printStream) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
        this.console = printStream;
        this.observers = new ArrayList<Observer>();
    }

    public void play() {
        for (int i = 0; i < noOfRounds; i++) {
            Move player1Move = player1.makeAMove();
            Move player2Move = player2.makeAMove();
            Score score = machine.calculateScore(player1Move, player2Move);
            player1.addScore(score.getPlayerOneScore());
            player2.addScore(score.getPlayerTwoScore());
            console.println(String.format("machine.Score %d %d", player1.getScore(), player2.getScore()));

            this.setChanged();
            this.notifyObservers(new Pair[]{new Pair<Player, Move>(player1, player1Move), new Pair<Player, Move>(player2, player2Move)});
        }
    }

}
