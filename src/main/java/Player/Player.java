package Player;

import move.*;

import java.io.BufferedReader;
import java.util.HashMap;

public class Player {

    private int score;
    private final MoveStrategy moveStrategy;

    public int getScore() {
        return this.score;
    }

    public Player(int score, MoveStrategy moveStrategy) {
        this.score = score;
        this.moveStrategy = moveStrategy;
    }

    public Move makeAMove() {
        return this.moveStrategy.makeAMove();
    }

    public void addScore(int i) {
        this.score += i;
    }


    public MoveStrategy getMoveStrategy(){
        return this.moveStrategy;
    }
}
