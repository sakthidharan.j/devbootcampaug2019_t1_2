package Player;

public enum PlayerType {
    CHEATER, COOPERATOR, MANUAL, COPYCAT;
}
