import Player.Player;
import Player.PlayerType;
import game.Game;
import machine.Machine;
import move.CheatMove;
import move.CooperativeMove;
import move.ManualMove;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Application {

    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        Player player1 = new Player(0,new ManualMove());
        Player player2 = new Player(0, new ManualMove());
        Machine machine = new Machine();

        new Game(player1, player2, machine, 3, System.out).play();

        //ALl cheat and coop
        player1 = new Player(0 , new CheatMove());
        player2 = new Player(0, new CooperativeMove());

        Game game = new Game(player1, player2, new Machine(), 5, System.out);
        game.play();
    }
}
