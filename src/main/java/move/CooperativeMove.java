package move;

public class CooperativeMove implements MoveStrategy {

    public Move makeAMove() {
        return Move.COOPERATE;
    }
}
