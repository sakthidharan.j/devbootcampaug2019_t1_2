package move;

import Player.Player;

import java.util.Observable;
import java.util.Observer;
import org.javatuples.Pair;


public class CopyCatMove implements MoveStrategy, Observer {

    Move lastMove;

    public CopyCatMove(){

        this.lastMove = Move.COOPERATE;
    }


    @Override
    public Move makeAMove() {
        return this.lastMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        Pair<Player, Move>[] moveList  = (Pair<Player, Move>[]) arg;

        for(Pair currPair: moveList){
            if ( ((Player) currPair.getValue0()).getMoveStrategy() != this){
                this.lastMove = (Move) currPair.getValue1();
                return;
            }
        }
    }
}
