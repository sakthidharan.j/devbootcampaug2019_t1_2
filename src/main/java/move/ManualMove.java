package move;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ManualMove implements MoveStrategy {

    InputStreamReader isr;
    BufferedReader bf;

    public ManualMove(){
        isr = new InputStreamReader(System.in);
        bf = new BufferedReader(isr);
    }

    public Move makeAMove() {
        String input = null;
        try {
            input = bf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(input.equals("coop")) {
            return Move.COOPERATE;
        }else{
            return Move.CHEAT;
        }
    }
}
