package move;

import java.io.BufferedReader;

public interface MoveStrategy {
    public Move makeAMove();
}
