package move;

import Player.Player;
import org.javatuples.Pair;

import java.util.Observable;
import java.util.Observer;

public class GrudgerMove implements MoveStrategy, Observer {

    Move lastMove;

    boolean notCheatedYet;

    public GrudgerMove() {
        this.lastMove = Move.COOPERATE;
    }

    @Override
    public void update(Observable o, Object arg) {

        Pair<Player, Move>[] moveList = (Pair<Player, Move>[]) arg;

        if (this.lastMove == Move.CHEAT) {
            return;
        }

        this.lastMove = this.getOpponentMove(moveList);

    }

    private Move getOpponentMove(Pair<Player, Move>[] moveList) {
        for (Pair currPair : moveList) {
            if (((Player) currPair.getValue0()).getMoveStrategy() != this) {
                return (Move) currPair.getValue1();
            }
        }

        return null;
    }

    @Override
    public Move makeAMove() {
        return this.lastMove;
    }


}
