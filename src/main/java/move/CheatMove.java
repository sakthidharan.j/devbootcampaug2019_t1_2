package move;

public class CheatMove implements MoveStrategy {
    public Move makeAMove() {
        return Move.CHEAT;
    }
}
